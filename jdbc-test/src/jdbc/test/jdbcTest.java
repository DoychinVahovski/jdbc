package jdbc.test;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class jdbcTest {
	public static void main(String[] args) throws SQLException {
		
		Connection myConn = null;
		Statement myStmt  = null;
		ResultSet myRs 	  = null;
		
		try {
			myConn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/demo?useSSL=false", "student", "student");
			System.out.println("Database connection successful!\n");
			
			myStmt = (Statement) myConn.createStatement();
			myRs   = myStmt.executeQuery("select * from employees");
			
			while ( myRs.next()) {
				System.out.println(myRs.getString("last_name") + ", " + myRs.getString("first_name"));
			}
		}
		
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}

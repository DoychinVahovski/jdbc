package jdbc.test;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class JdbcInsertDemo {
	public static void main(String[] args) throws SQLException {
		
//		Connection myConn = null;
//		Statement myStmt = null;
//		ResultSet myRs = null;
//		
//		String dbUrl = "jdbc:mysql//localhost:3306/demo?useSSL=false";
//				//		jdbc:mysql//localhost:3306/demo?useSSL=false	
//		String user = "student";
//		String pass = "student";
	    
	    Connection myConn = null;
        Statement myStmt  = null;
        ResultSet myRs    = null;
        String user = "student";
        String pass = "student";
        String dbUrl = "jdbc:mysql://localhost:3306/demo?useSSL=false";
		
		try {
		    myConn = (Connection) DriverManager.
                    getConnection(dbUrl, 
                                  user, 
                                  pass);
		    System.out.println("Database connection successful!\n");
			myStmt = (Statement) myConn.createStatement();
			System.out.println("Inserting a new employee to database\n");
			
			int rowsAffected = myStmt.executeUpdate("Insert into employees" + "(last_name, first_name, email, department, salary)" + "values" + "('Wright' , 'Eric', 'eric.wright@foo.com', 'HR', 33000.00)");
			myRs = myStmt.executeQuery("slect * from employees order by last_name");
		
			while(myRs.next()) {
				System.out.println(myRs.getString("last_name") + ", " + myRs.getString("first_name"));
			}
		}
		
		catch(Exception exc) {
			exc.printStackTrace();
		}
		
		finally {
			if (myRs != null) {
				myRs.close();
			}
		}
	}
}
